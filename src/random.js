const signal = () => Math.random() < 0.5 ? -1 : 1
const number = (num = 100, alwaysPositive = false) => alwaysPositive ? Math.round((Math.random()) * num) : Math.round(((Math.random() * signal()) * num))

module.exports = number