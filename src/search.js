const axios = require('axios');

const geonamesBaseURL = 'http://api.geonames.org'
const geonamesUsername = '&username=fernandopassoz'
const geonamesCountry = '&country=BR'

const searchAllCountries = (req, res, next) => {
  axios
    .get(`${geonamesBaseURL}/searchJSON?${geonamesUsername}&maxRows=300&featureCode=PCLS&featureCode=PCLI&featureCode=PCLF&featureCode=PCLD`)
    .then(result => {
      const countries = []
      result
        .data
        .geonames
        .forEach(element => {
          countries.push({"country": element.name, "code": element.countryCode, "latitude": element.lat, "longitude": element.lng})
        });
      countries.sort((a, b) => {
        var x = a.country
        var y = b.country
        if (x < y) {
          return -1
        }
        if (x > y) {
          return 1
        }
        return 0
      })
      res.countries = countries
      next()
    })
    .catch(err => {
      res.countries = err.toString()
      next()
    })
}

const searchCities = (req, res, next) => {
  const city = 'Rio de Janeiro'

  axios
    .get(`${geonamesBaseURL}/searchJSON?${geonamesUsername}${geonamesCountry}&name=${req.params.city || city}&featureCode=ADM2`)
    .then((result) => {

      const cities = []
      result
        .data
        .geonames
        .forEach(element => {
          const obj = {}
          obj.countryCode = element.countryCode
          obj.countryName = element.countryName
          obj.stateCode = element.adminCodes1.ISO3166_2
          obj.stateName = element.adminName1
          obj.cityName = element.toponymName
          obj.latitude = element.lat
          obj.longitude = element.lng

          cities.push(obj)
        });

      res.cities = cities
      next()
    })
    .catch((err) => {
      res.cities = err
      next()
    })

}

const searchByCoordinates = (req, res, next) => {

  axios
    .get(`${geonamesBaseURL}/findNearbyJSON?${geonamesUsername}&lat=${req.params.latitude}&lng=${req.params.longitude}&radius=${req.params.radius}`)
    .then(result => {
      res.places = result.data.geonames
      next()
    })
    .catch(err => {
      res.places = err
      next()
    })

}

const searchRadius = (req, res, next) => {

  axios
    .get(`${geonamesBaseURL}/searchJSON?${geonamesUsername}${geonamesCountry}&name=${req.params.place}&featureCode=ADM2`)
    .then(result => {
      const latitude = result.data.geonames[0].lat
      const longitude = result.data.geonames[0].lng
      axios
        .get(`${geonamesBaseURL}/findNearbyJSON?${geonamesUsername}&lat=${latitude}&lng=${longitude}&radius=${req.params.radius}`)
        .then(result => {
          res.places = result.data.geonames
          next()
        })
        .catch(err => {
          res.places = err
          next()
        })
    })
    .catch(err => {
      res.places = err
      next()
    })

}

module.exports = {
  searchCities,
  searchByCoordinates,
  searchRadius,
  searchAllCountries
}