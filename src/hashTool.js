const bcrypt = require('bcrypt');

const salt = bcrypt.genSaltSync(8)

const hashMiddleware = function (req, res, next) {
  if (req.body.password) {
    req.body.password = bcrypt.hashSync(req.body.password, salt);
  }

  next();
}

const hashPassword = function (password) {
  return bcrypt.hashSync(password, salt);
}

module.exports = {
  hashMiddleware,
  hashPassword
}