const validateEmail = (email) => email.match(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/) ? true : false

module.exports = validateEmail
