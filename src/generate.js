const caracter = () => Math.random() < 0.5 ? Math.random().toString(36).substring(2,3) : Math.random().toString(36).substring(2,3).toUpperCase()
const generate = (length = 10) => {
    let str = ''
    for(let i = 0;i<length;i++){
        str += caracter()
    }
    return str
}

module.exports = generate